package com.RoutePlanner;

public class RouteDetails {
	private String From;
	private String To;
	private String Distance;
	private String Travel_Time;
	private String Airfare;
	RouteDetails(String From,String To,String Distance,String Travel_Time,String Airfare){
		this.From=From;
		this.Distance=Distance;
		this.To=To;
		this.Travel_Time=Travel_Time;
		this.Airfare=Airfare;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getDistance() {
		return Distance;
	}
	public void setDistance(String distance) {
		Distance = distance;
	}
	public String getTravel_Time() {
		return Travel_Time;
	}
	public void setTravel_Time(String travel_Time) {
		Travel_Time = travel_Time;
	}
	public String getAirfare() {
		return Airfare;
	}
	public void setAirfare(String airfare) {
		Airfare = airfare;
	}
	
}


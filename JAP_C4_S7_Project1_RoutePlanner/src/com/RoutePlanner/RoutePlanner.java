package com.RoutePlanner;
import com.RoutePlanner.*;
import java.util.*;
import java.io.*;

public class RoutePlanner{
	public void showAllConnections(RouteDetails[] arr,String from,String to)
	{
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i].getFrom().equals(from) && arr[i].getTo().equals(to))
			{
				System.out.printf("%13s %15s %18s %13s %25s",arr[i].getFrom(),arr[i].getTo(),arr[i].getDistance(),arr[i].getTravel_Time(),arr[i].getAirfare());
				System.out.println();
			}
		}
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i].getFrom().equals(from))
			{
				for(int j=0;j<arr.length;j++)
				{
					if(arr[i].getTo().equals(arr[j].getFrom()) && arr[j].getTo().equals(to))
					{
						System.out.printf("%13s %15s %18s %13s %25s",arr[i].getFrom(),arr[i].getTo(),arr[i].getDistance(),arr[i].getTravel_Time(),arr[i].getAirfare());
						System.out.println();
						System.out.printf("%13s %15s %18s %13s %25s",arr[j].getFrom(),arr[j].getTo(),arr[j].getDistance(),arr[j].getTravel_Time(),arr[j].getAirfare());
						System.out.println();
					}
				}
			}
		}
	}
	public void sortDirectFlights(RouteDetails[] arr,String fromCity,int cnt)
	{
		String[] arr1=new String[cnt];
		for(int i=0,j=0;i<arr.length;i++)
		{
			if(arr[i].getFrom().equals(fromCity))
			{
				arr1[j]=arr[i].getTo();
				j++;
			}
		}
		Arrays.sort(arr1);
		for(int i=0;i<arr1.length;i++)
		{
			for(int j=0;j<arr.length;j++)
			{
				if(arr[j].getFrom().equals(fromCity) && arr[j].getTo().equals(arr1[i]))
				{
					System.out.printf("%13s %15s %18s %13s %25s",arr[j].getFrom(),arr[j].getTo(),arr[j].getDistance(),arr[j].getTravel_Time(),arr[j].getAirfare());
					System.out.println();
				}
			}
		}
	}
	public int showDirectFlights(RouteDetails[] arr,String fromCity) {
		int cnt=0;
		for(int i=0;i<arr.length;i++)
		{
			if(arr[i].getFrom().equals(fromCity))
			{
				System.out.printf("%13s %15s %18s %13s %25s",arr[i].getFrom(),arr[i].getTo(),arr[i].getDistance(),arr[i].getTravel_Time(),arr[i].getAirfare());
				System.out.println();
				cnt++;
			}
		}
		if(cnt==0)
		{
			System.out.println("There are no direct flights");
		}
		return cnt;
		
	}
	public RouteDetails[] readFile(String filepath,int count)
	{
		RouteDetails[] arr=new RouteDetails[count];
		try {
			BufferedReader br1=new BufferedReader(new FileReader(filepath));
			String line=br1.readLine();
			for(int i=0;i<count;i++)
			{
				line=br1.readLine();
				String[] values=line.split(",");
				String From=values[0];
				String To=values[1];
				String Distance=values[2];
				String Travel_Time=values[3];
				String AirFare=values[4];
				RouteDetails rp1=new RouteDetails(From, To, Distance, Travel_Time, AirFare);
				arr[i]=rp1;
			}
			br1.close();
		}
		catch(Exception e) {
	}
		return arr;
	}
	public int countlines(String filePath){
		int cnt=-1;
		try {
		BufferedReader br=new BufferedReader(new FileReader(filePath));
		String line;
		while((line=br.readLine())!=null)
		{
			cnt++;
		}
		br.close();
		}
		catch(Exception e){
		}
		return cnt;
	}
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int cnt=0,dcnt;
		String Filepath="input.txt";
		RoutePlanner rp= new RoutePlanner();
		cnt=rp.countlines(Filepath);
		RouteDetails[] arr1=new RouteDetails[cnt];
		arr1=rp.readFile(Filepath, cnt);
		System.out.println("Route Details");
		System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------------------");
		for(int i=0;i<arr1.length;i++)
		{
			System.out.printf("%13s %15s %18s %13s %25s",arr1[i].getFrom(),arr1[i].getTo(),arr1[i].getDistance(),arr1[i].getTravel_Time(),arr1[i].getAirfare());
			System.out.println();
		}
		System.out.println("Enter From City: ");
		String fromcity1=sc.nextLine();
		String fromcity=fromcity1.substring(0,1).toUpperCase()+fromcity1.substring(1);
		System.out.println();
		System.out.println("Direct Flights From "+fromcity+" are: ");
		System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------------------");
		dcnt=rp.showDirectFlights(arr1, fromcity);
		if(dcnt>0)
		{
			System.out.println();
			System.out.println("Sorted Direct Flight Routes From "+fromcity+" are: ");
			System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
			System.out.println();
			System.out.println("---------------------------------------------------------------------------------------------------");
			rp.sortDirectFlights(arr1, fromcity, dcnt);
		}
		else {
			System.out.println("Nothing to Sort");
		}
		System.out.println("Enter from city for Intermediate Flights: ");
		String from1=sc.nextLine();
		String from=from1.substring(0,1).toUpperCase()+from1.substring(1);
		System.out.println("Enter To city for Intermediate flights");
		String to1=sc.nextLine();
		String to=to1.substring(0,1).toUpperCase()+to1.substring(1);
		System.out.println();
		System.out.println("Intermediate Flight Details: ");
		System.out.printf("%12s %14s %20s %19s %18s","From", "To", "Distance", "Travel Time", "Airfare"); 
		System.out.println();
		System.out.println("---------------------------------------------------------------------------------------------------");
		rp.showAllConnections(arr1, from, to);
	}

}
